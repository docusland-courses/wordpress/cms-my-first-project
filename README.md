# CMS-my-first-project

Vous avez le plaisir de pouvoir choisir le thème de votre choix. 

Réaliser une application en utilisateur le CMS Wordpress. 
Ce dernier devra contenir à minima les pages suivantes : 

![Wireframe](./wireframe.png)

# L'attendu minimum du site

## La Homepage
Une page d'accueil doit d'une exiter sur ce site. Ce que nous appelons communément la landing page. 
Il s'agit page présentant le rôle et le but du site. 


## Les catégories d'articles
Votre site devra contenir des articles. 
Les articles devront être associés à une ou plusieurs catégories.
Ces catégories seront organisées en arborescence.

## L'affichage d'une catégorie.
Lorsque l'utilisateur visualiser une catégorie d'articles, il trouvera également une barre de recherche, permettant de filtrer les articles affichés au sein de cette catégorie.

## Le formulaire de contact
Le formulaire de contact doit envoyer une email à l'administrateur du site. 
Ce formulaire devra contenir les champs suivants : 
 - Nom (texte court)
 - Email (type email)
 - Objet du mail (texte court)
 - Avis sur le site (Note allant de 1 à 10)
 - Commentaire (texte long)

Ce formulaire devra également contenir un captcha

## Le Menu
Au sein du menu se trouvera : 
 - Un logo : Un click dessus ramène l'utilisateur sur la page d'accueil du site.
 - Au moins un lien vers une catégorie parente. 
 - Avec des sous menu pour les catégories enfants.



## Le pied de page
Le pied de page devra contenir les mentions légales du site.


# Livrable
Le livrable doit être réalisé sur le serveur de l'école et une URL doit être communiquée au formateur. 
Un compte utilisateur doit également être fourni au formateur. 

Votre livrable doit être responsive.

## Bonus possibles : 
- Thème enfant
- Esthétique avancé
- Caroussel

# Le Barème
- Site livré sur le serveur : 2 pts
- Site responsive : 2 pts
- Barre de recherche : 2 pts
- Barre de menu : 2 pts
- Formulaire de contact : 4 pts
- Site Esthétique : 4 pts
- Respect des consignes : 4 pts
- Bonus : 4pts
